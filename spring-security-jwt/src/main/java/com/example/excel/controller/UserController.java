package com.example.excel.controller;


import com.example.excel.config.JwtService;
import com.example.excel.config.UserInfoService;
import com.example.excel.model.AuthRequest;
import com.example.excel.model.Page;
import com.example.excel.news.PageService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/auth")
public class UserController {

    @Value("${auth.api.baseurl}")
    private String authApiBaseUrl;

    @Value("${jwt.secret}")
    private String SECRET;


    @Autowired
    private UserInfoService service;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private PageService pageService;
    @Autowired
    private UserInfoService userInfoService;


//        @PostMapping("/addNewUser")
//    public String addNewUser(@RequestBody UserInfo userInfo) {
//        return service.addUser(userInfo);
//    }

    @GetMapping("/user/userProfile")
    public String userProfile(HttpServletRequest request) {
        String username = getUsernameFromRequest(request);
        if (username == null || username.isEmpty()) {
            return "User not authenticated";
        }
        int userId = service.getUserIdByUsername(username);

        if (userId <= 0) {
            return "Access Denied";
        }
        boolean hasAccess = pageService.hasAccess(userId, "user");
        if (hasAccess) {
            return "Hello User!";
        } else {
            return "Access Denied";
        }
    }

    @GetMapping("/admin/adminProfile")
    public String adminProfile(HttpServletRequest request) {
        String username = getUsernameFromRequest(request);

        if (username == null || username.isEmpty()) {
            return "User not authenticated";
        }
        int userId = service.getUserIdByUsername(username);
        if (userId <= 0) {
            return "Access Denied";
        }
        boolean hasAccess = pageService.hasAccess(userId, "admin");
        if (hasAccess) {
            return "Hello Admin!";
        } else {
            return "Access Denied";
        }
    }


    @PostMapping("/generateToken")
    public String authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authentication.isAuthenticated()) {
            return jwtService.generateToken(authRequest.getUsername());
        } else {
            throw new UsernameNotFoundException("invalid user request !");
        }
    }

    @PostMapping("/setUserAccess")
    public ResponseEntity<String> setUserAccess(@RequestBody Page page) {
        try {
            if (page.getUserId() <= 0 || page.getPageName() == null || page.getPageName().isEmpty()) {
                return ResponseEntity.badRequest().body("Invalid input data");
            }

            Page existingPage = pageService.getPageByUserIdAndPageName(page.getUserId(), page.getPageName());

            if (existingPage == null) {
                // Trang không tồn tại, tạo mới và lưu vào cơ sở dữ liệu
                existingPage = new Page();
                existingPage.setUserId(page.getUserId());
                existingPage.setPageName(page.getPageName());
            }

            existingPage.setAllowedAccess(page.getAllowedAccess());
            pageService.savePage(existingPage);

            return ResponseEntity.ok("User access to " + page.getPageName() + " page is set to " + (page.getAllowedAccess() ? "ALLOWED" : "DENIED"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error while setting user access: " + e.getMessage());
        }
    }


    private String getUsernameFromRequest(HttpServletRequest request) {
        // Ví dụ: Lấy thông tin username từ token JWT trong header "Authorization"
        String authorizationHeader = request.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);
            String username = null;
            try {
                Claims claims = Jwts.parserBuilder()


                        .setSigningKey(SECRET)
                        .build()
                        .parseClaimsJws(token)
                        .getBody();

                username = claims.getSubject();

                if (username != null) {
                    return username;
                }
            } catch (Exception e) {
                // xử lý lỗi khi có ngoại lệ xảy ra
            }
        }

        return null;
    }

}


