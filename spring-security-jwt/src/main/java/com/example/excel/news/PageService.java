package com.example.excel.news;

import com.example.excel.model.Page;
import com.example.excel.service.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// PageService.java
@Service
public class PageService {

    private final PageRepository pageRepository;

    @Autowired
    public PageService(PageRepository pageRepository) {
        this.pageRepository = pageRepository;
    }

    public boolean hasAccess(int userId, String pageName) {
        Page page = pageRepository.findByUserIdAndPageName(userId, pageName);
        return page != null && page.getAllowedAccess();
    }

    public Page getPageByUserIdAndPageName(int userId, String pageName) {
        return pageRepository.findByUserIdAndPageName(userId, pageName);
    }

    public void savePage(Page page) {
        pageRepository.save(page);
    }
}

