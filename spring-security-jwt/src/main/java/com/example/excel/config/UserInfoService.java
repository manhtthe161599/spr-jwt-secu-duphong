package com.example.excel.config;

import com.example.excel.model.Users;
import com.example.excel.service.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserInfoService implements UserDetailsService {

    @Autowired
    private UserInfoRepository repository;

//    @Autowired
//    private PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Users> userDetail = repository.findByName(username);

        // Converting userDetail to UserDetails
        return userDetail.map(UserInfoDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("User not found " + username));
    }

    public int getUserIdByUsername(String username) {
        Optional<Users> userInfoOptional = repository.findByEmail(username);
        if (userInfoOptional.isPresent()) {
            Users userInfo = userInfoOptional.get();
            return userInfo.getId();
        }
        return -1; // Trả về giá trị mặc định hoặc xử lý nếu không tìm thấy người dùng
    }






//    public String addUser(UserInfo userInfo) {
//        userInfo.setPassword(encoder.encode(userInfo.getPassword()));
//        repository.save(userInfo);
//        return "User Added Successfully";
//    }


}


