package com.example.excel.service;


import com.example.excel.model.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepository extends JpaRepository<Page, Integer> {
    Page findByUserIdAndPageName(int userId, String pageName);

}

